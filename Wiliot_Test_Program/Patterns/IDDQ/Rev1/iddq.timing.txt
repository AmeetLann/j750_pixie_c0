DFF 1.1	Time Sets (Basic)															
																
	Timing Mode:	extended														
																
		Cycle		Pin/Group		Data		Drive				Compare				
	Time Set	Period	CPP	Name	Setup	Src	Fmt	On	Data	Return	Off	Mode	Open	Close	Comment	
	T0	=_tcyc_T0		VDD_EXTREME_TX	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		VDD_CRG	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		GPIO4	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		TCK	i/o	PAT	RH	=0*_tcyc_T0	=0*_tcyc_T0	=0.5*_tcyc_T0	=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		TDI	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		TMS	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		TDO	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.505*_tcyc_T0				
	T0	=_tcyc_T0		GPIO0	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		GPIO1	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		GPIO2	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		GPIO3	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		GPIO5	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		GPIO6	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T0	=_tcyc_T0		VDD_DBG	i/o	PAT	NR	=0*_tcyc_T0	=0*_tcyc_T0		=1*_tcyc_T0	Edge	=0.5*_tcyc_T0				
	T1	=_tcyc_T1		VDD_EXTREME_TX	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		VDD_CRG	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		GPIO4	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		TCK	i/o	PAT	RH	=0*_tcyc_T1	=0*_tcyc_T1	=0.5*_tcyc_T1	=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		TDI	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		TMS	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		TDO	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.505*_tcyc_T1				
	T1	=_tcyc_T1		GPIO0	i/o	PAT	NR	=0*_tcyc_T1	=0.5*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		GPIO1	i/o	PAT	NR	=0*_tcyc_T1	=0.5*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		GPIO2	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		GPIO3	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		GPIO5	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		GPIO6	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
	T1	=_tcyc_T1		VDD_DBG	i/o	PAT	NR	=0*_tcyc_T1	=0*_tcyc_T1		=1*_tcyc_T1	Edge	=0.5*_tcyc_T1				
